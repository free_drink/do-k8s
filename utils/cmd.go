package main

import (
	"flag"
	"fmt"
	"os"

	jobgenerator "gitlab.com/freedrink/utils/jobGenerator"
)

func main() {
	genCommand := flag.NewFlagSet("gen", flag.ExitOnError)
	genUser := genCommand.String("user", "", "User name")
	genName := genCommand.String("name", "", "Name of the job")
	genURL := genCommand.String("url", "", "URL for CDN")
	genJob := genCommand.String("job", "insta", "Job type")
	flag.Parse()

	if len(os.Args) < 2 {
		fmt.Println("Please provide a command: gen, remove, or get")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "gen":
		genCommand.Parse(os.Args[2:])
		jobgenerator.GenJob(*genUser, *genName, *genURL, *genJob)
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}
}
