package jobgenerator

import (
	"flag"
	"fmt"
	"os"
	"text/template"
	"time"

	"github.com/google/uuid"
)

// InstaJob represents the structure of the templated file
type InstaJob struct {
	Name      string
	User      string
	URL_CDN   string
	ExpiresAt string
}

func GenJob(user, name, url, job string) {
	now := time.Now()
	verifyFlags(user, url, &name)

	instaJob := InstaJob{
		Name:      name,
		User:      user,
		URL_CDN:   url,
		ExpiresAt: *calculateExpiry(now),
	}

	// Load the template from file
	t, err := template.ParseFiles(fmt.Sprintf("jobGenerator/templates/%s_job.tmpl", job))
	if err != nil {
		panic(err)
	}

	// Execute the template with InstaJob data
	err = t.Execute(os.Stdout, instaJob)
	if err != nil {
		panic(err)
	}
}

func calculateExpiry(t time.Time) *string {
	timeString := t.Add(6 * time.Hour).Format(time.RFC3339)
	return &timeString
}

func verifyFlags(user, url string, name *string) {
	if user == "" || url == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	if *name == "" {
		uuidWithHyphen := uuid.New()
		*name = uuidWithHyphen.String()
	}
}
