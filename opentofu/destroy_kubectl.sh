#!/bin/bash

# Cluster name passed as the first argument
CLUSTER_NAME=$1

# Check if the cluster configuration exists
if kubectl config get-clusters | grep -q "^${CLUSTER_NAME}\$"; then
  kubectl config delete-cluster ${CLUSTER_NAME}
else
  echo "No cluster named ${CLUSTER_NAME} found in kubectl config."
fi