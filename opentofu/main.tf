// env var DIGITALOCEAN_TOKEN is required
// doctl and kubectl are required

terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = ">= 2.0.0"
    }
  }
  required_version = ">= 0.12"
}

provider "digitalocean" {
}

resource "digitalocean_kubernetes_cluster" "supercluster" {
    name = "k8s-1-29-1-do-0-fra1-1713384921754"
    region = "fra1"
    version = "1.29.1-do.0"

    node_pool {
        name = "pool-80ad1pwg8"
        size = "s-2vcpu-4gb"
        node_count = 3
    }

    provisioner "local-exec" {
        when    = create
        command = "${path.module}/configure_kubectl.sh ${self.name}"
    }

    provisioner "local-exec" {
        when    = destroy
        command = "${path.module}/destroy_kubectl.sh ${self.name}"
    }

}

output "cluster_endpoint" {
  value = digitalocean_kubernetes_cluster.supercluster.endpoint
  description = "The endpoint of the Kubernetes cluster."
}

output "cluster_version" {
  value = digitalocean_kubernetes_cluster.supercluster.version
  description = "The version of the Kubernetes cluster."
}