# Dkron Application Deployment Guide

This document provides instructions on deploying and managing the Dkron application on Kubernetes. Dkron is a fault-tolerant, distributed job scheduling system that ensures your scheduled jobs are managed efficiently.

## Prerequisites

Before proceeding, ensure the following requirements are met:
- A running Kubernetes cluster, see [previous article](docs/do_l8s_cluster.md).
- The `kubectl` command-line tool, configured to communicate with your cluster.

## Deployment Steps

### 1. Deploy the Dkron Application

To deploy Dkron, apply the Kubernetes configuration file which includes the necessary Deployment, Service, and other resource definitions.

```bash
kubectl apply -f k8s/dkron.yml
```

### 2. Accessing Dkron

To access the Dkron service, first determine the external IP address of the Dkron server:

```bash
EXTERNAL_DKRON_IP=$(kubectl get service dkron-server -o jsonpath='{.status.loadBalancer.ingress[*].ip}')
```

### 3. Create a Job

Create a new job in Dkron using a `curl` command that sends a POST request to the Dkron API:

```bash
curl --location --request POST "http://${EXTERNAL_DKRON_IP}:8080/v1/jobs" \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "supermegajob2",
    "schedule": "@every 5s",
    "concurrency": "forbid",
    "executor": "shell",
    "executor_config": {
      "command": "echo '\''hi'\''"
    }
}'
```

This job, named `supermegajob2`, is scheduled to run every 5 seconds. It executes a simple `echo` command, with the concurrency setting "forbid" to ensure that the job does not run concurrently.

Aditionally, to create a new job in Dkron, you can utilize the provided utility `job_generator.go` to generate the job JSON from a template.
   
   ```bash
   go run utils/job_generator.go -job <job_type> -name <job_name> -user <user_name> -url <cdn_url>
   ```

This will the placeholders `{{.Name}}, {{.User}}, {{.URL_CDN}}, {{.ExpiresAt}}` with the appropriate values for your job in the template file `<job_type>_job.tmpl`


## Deprovisioning

To safely deprovision and remove the Dkron deployment from your Kubernetes cluster, run:

```bash
kubectl delete -f k8s/dkron.yml
```