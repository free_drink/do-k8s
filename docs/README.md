# Articles
1. **Set up the Kubernetes cluster:**
   Navigate to the [Kubernetes Cluster Setup Guide](docs/do_k8s_cluster.md) for detailed instructions on how to deploy your Kubernetes cluster using DigitalOcean.

2. **Deploy the Dkron application:**
   Follow the steps in the [Dkron Deployment Guide](docs/dkron.md) to deploy and manage the Dkron job scheduling application on your cluster.