# Kubernetes Cluster on DigitalOcean

This OpenTofu configuration facilitates the setup of a Kubernetes cluster using DigitalOcean's managed Kubernetes service. It aims to provide a straightforward process for deploying a cluster.

## Prerequisites

- **tofu v0.12** or higher.
- **doctl**: DigitalOcean's command-line tool. Follow the [official installation guide](https://docs.digitalocean.com/reference/doctl/how-to/install/).
- **kubectl**: Install kubectl following the [Kubernetes documentation](https://kubernetes.io/docs/tasks/tools/).
- **DigitalOcean API Token**: Set the `DIGITALOCEAN_TOKEN` environment variable with your DigitalOcean personal access token, which can be generated from the DigitalOcean control panel.

## Setup

1. **Clone the Repository**:
    ```bash
    git clone https://gitlab.com/free_drink/do-k8s.git
    cd do-k8s/opentofu
    ```

2. **Initialize Tofu**:
    ```bash
    tofu init
    ```

3. **Plan Tofu Execution**:
    ```bash
    tofu plan
    ```

4. **Apply the Configuration**:
    ```bash
    tofu apply -auto-approve
    ```

5. **Destroy the Cluster**:
    ```bash
    tofu destroy
    ```

The OpenTofu local provisioner manages authentication during cluster creation and removes the cluster from kubeconfig upon decommission.

## Outputs

- **Cluster Endpoint**: The endpoint URL of the Kubernetes cluster, accessible via kubectl.
- **Cluster Version**: The specific version of the Kubernetes cluster that is deployed.

Continue with the deployment of the Dkron application by following the guide in [this article](docs/dkron.md).