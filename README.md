# Kubernetes Cluster and Dkron Deployment on DigitalOcean

This repository provides comprehensive guides for setting up a Kubernetes cluster and deploying the Dkron application on DigitalOcean's managed Kubernetes service.

## Contents

- [Kubernetes Cluster Setup](docs/do_k8s_cluster.md): Instructions for setting up a Kubernetes cluster using DigitalOcean.
- [Dkron Deployment Guide](docs/dkron.md): Steps for deploying and managing the Dkron application on your Kubernetes cluster.

## Prerequisites

Ensure you have the following tools installed:
- `tofu` v0.12 or higher
- `doctl`: DigitalOcean's command-line tool
- `kubectl`: The Kubernetes command-line tool
- DigitalOcean API Token: Set up the `DIGITALOCEAN_TOKEN` environment variable with your token.

## Quick Start

1. **Set up the Kubernetes cluster:**
   Navigate to the [Kubernetes Cluster Setup Guide](docs/do_k8s_cluster.md) for detailed instructions on how to deploy your Kubernetes cluster using DigitalOcean.

2. **Deploy the Dkron application:**
   Follow the steps in the [Dkron Deployment Guide](docs/dkron.md) to deploy and manage the Dkron job scheduling application on your cluster.

## Project Layout

```
.
├── README.md                  
├── docs/                      
│   ├── dkron.md               
│   └── do_k8s_cluster.md      
├── k8s/                       
│   └── dkron.yml              
├── opentofu
│   ├── configure_kubectl.sh
│   ├── destroy_kubectl.sh
│   └── main.tf
└── utils            
```

- **`README.md`**: This file provides an overview of the project, setup instructions, and other important information.
- **`docs/`**: Contains all the documentation related to the project. It includes detailed guides on how to deploy the Kubernetes cluster and manage the Dkron application.
- **`k8s/`**: This directory holds all Kubernetes-related configuration files, specifically tailored for deploying applications like Dkron. It helps in maintaining separation between infrastructure code and application configuration.
- **`opentofu/`**: Dedicated to the OpenTofu configuration files and scripts used for setting up and managing the infrastructure on DigitalOcean. This includes scripts for setting up kubernetes cluster authentication, applying Terraform configurations, and safely destroying the environment when necessary.
- **`utils/`**: This directory contains utility scripts and files that assist in various tasks such as job generation, debugging, and other auxiliary functions.


## Important Notes

- **Resource Cleanup:** Before decommissioning the Kubernetes cluster on DigitalOcean, ensure that all resources related to the Dkron deployment have been properly deleted. This helps prevent any orphaned resources or dangling charges.